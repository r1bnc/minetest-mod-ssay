minetest.register_privilege("unisay", "Allows say as server")
minetest.register_chatcommand("ssay", {
  description = "Say message as a server",
  params = "<message>",
  privs = {unisay=true},
  func = function(name, param)
    minetest.chat_send_all(minetest.colorize("#F00","server")..": "..param)
  end,
})


minetest.register_chatcommand("rawsay", {
  description = "Say raw text in the chat",
  params = "<text>",
  privs = {unisay=true},
  func = function(name, param)
    minetest.chat_send_all(param)
  end,
})
